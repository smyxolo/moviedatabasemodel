import java.time.LocalDate;
import java.util.HashMap;

public class MovieDatabase {
    private HashMap<String, Movie> movieMapByName = new HashMap<>();

    public void addMovie(String title, MovieType TYPE, LocalDate releaseDate, String director){
        movieMapByName.put(title, new Movie(title, TYPE, releaseDate, director));
    }

    public Movie seachMovie(String name){
        return movieMapByName.get(name);
    }

    public void printAllMovies(){
        for (Movie movie : movieMapByName.values()) {
            System.out.println(movie);
        }
    }

    public void printAllMovies(MovieType TYPE){
        for (Movie movie : movieMapByName.values()) {
            if (movie.getTYPE().equals(TYPE)){
                System.out.println(movie);
            }
        }
    }

    public HashMap<String, Movie> getMovieMapByName() {
        return movieMapByName;
    }
}
