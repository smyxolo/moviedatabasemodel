import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;
import java.util.Date;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        String inputLine = "";
        boolean isWorking = true;
        MovieDatabase movieDatabase = new MovieDatabase();

        printOptions();

        while (isWorking){
            inputLine = skan.nextLine();
            try {
                String[] inputArguments = inputLine.split(" ", 5);
                if (inputArguments[0].equals("quit")){
                    isWorking = false;
                }
                else if(inputArguments[0].equals("add")){
                    String movieTitle = inputArguments[4];
                    MovieType TYPE = MovieType.valueOf(inputArguments[1].toUpperCase());
                    DateTimeFormatter dtf = new DateTimeFormatterBuilder().appendPattern("yyyy")
                            .parseDefaulting(ChronoField.MONTH_OF_YEAR, 1)
                            .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
                            .toFormatter();
                    LocalDate releaseDate = LocalDate.parse(inputArguments[2], dtf);
                    String director = inputArguments[3];
                    movieDatabase.addMovie(movieTitle, TYPE, releaseDate, director);
                    System.out.println("Movie added.");
                }
                else if (inputArguments[0].equals("search")){
                    String name = inputArguments[1];
                    if(movieDatabase.getMovieMapByName().containsKey(name)){
                        System.out.println(movieDatabase.seachMovie(name));
                    }
                    else {
                        System.out.println("This movie does not exist in our database.");
                    }
                }
                else if(inputArguments[0].equals("showAll")){
                    movieDatabase.printAllMovies();
                }
                else if(inputArguments[0].equals("showType")){
                    MovieType TYPE = MovieType.valueOf(inputArguments[1]);
                    movieDatabase.printAllMovies(TYPE);
                }
                else if(inputArguments[0].equals("options")){
                    printOptions();
                }
                else System.out.println("Wrong command.");
            }catch (ArrayIndexOutOfBoundsException aioobe){
                System.out.println("Not enough arguments");
            }catch (IllegalArgumentException iae){
                System.out.println("Incorrect movieType");
            }catch (DateTimeParseException dtpe){
                System.out.println("Incorrect date format (required 'yyyy').");
            }
        }
    }

    public static void printOptions(){
        System.out.println("Choose on of the following options:");
        System.out.println("-> add + movieType[ACTION/DRAMA/COMEDY/HORROR] + releaseDate(yyyy) + director + movieTitle");
        System.out.println("-> search + movieTitle");
        System.out.println("-> showAll");
        System.out.println("-> showType + movieType");
        System.out.println("-> options (to reprint this information)");
        System.out.println("-> quit (to terminate the program)");
    }
}
