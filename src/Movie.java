import java.time.LocalDate;

public class Movie {
    private String title;
    private MovieType TYPE;
    private LocalDate releaseDate;
    private String director;

    public Movie(String title, MovieType TYPE, LocalDate releaseDate, String director) {
        this.title = title;
        this.TYPE = TYPE;
        this.releaseDate = releaseDate;
        this.director = director;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public MovieType getTYPE() {
        return TYPE;
    }

    public void setTYPE(MovieType TYPE) {
        this.TYPE = TYPE;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", TYPE=" + TYPE +
                ", releaseDate=" + releaseDate +
                ", director='" + director + '\'' +
                '}';
    }
}


